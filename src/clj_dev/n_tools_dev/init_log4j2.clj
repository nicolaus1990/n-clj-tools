(ns n-tools-dev.init-log4j2
  (:require [n-tools-dev.log4j2 :as tl]
            [clojure.tools.logging :as log]))

;; Init logging:
(do (tl/setup-log-config :log-level "debug"
                         :log-dir "/tmp/logs/n-clj-tools/"
                         :log4j2xml-sys-var-log-dir "log_directory"
                         :log4j2xml-sys-var-log-level "log_level")
    (log/info "Init log4j2 done!"))

#_(comment
    "To be required like:"
    (ns some-namespace
      (:require [n-tools-dev.init-log4j2]
                ...))
  )
