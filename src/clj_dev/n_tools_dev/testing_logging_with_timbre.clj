(ns n-tools-dev.testing-logging-with-timbre
  (:require [n-tools-dev.log-with-timbre :as lt]
            [taoensso.timbre :as log])
  #_(:import [org.apache.logging.log4j Logger]
           [org.apache.logging.log4j LogManager]))

;;--------------------------------------------------------
;; For trying out logging
;; Example of using taoensso's timbre logging lib.
;;
;; Logging levels:
;;       ALL < DEBUG < INFO < WARN < ERROR < FATAL < OFF
;;--------------------------------------------------------

(defn -main [& args]
  (do (lt/config-log! :debug ; <--- :log-level 
                      true   ; <--- :log-write-to-file
                      )
      (log/info "Hello from timbre!")))




