(ns n-tools-dev.core
  (:require [n-tools-dev.init-log4j2]
            ;;[n-tools.core :as pcore]
            [n-tools-dev.testing-logging :as pcore]
            [clojure.tools.logging :as log]))

(comment "This file is intended to show how to initialize logging with log4j2.

- In lein profiles, add main
- This file...
  - requires, before anything else, [n-tools-dev.init-log4j2] dependency, which initilizes logging.
  - calls the main function from the production code, after initializing logging.
  - will not be in production code (add path to this file to source files and :main n-tools-dev.core, 
    all in a lein development profile)
")

(defn -main [& args]
  (do (log/info "*********** DEBUG MODE ***********")
      (apply pcore/-main args)))
