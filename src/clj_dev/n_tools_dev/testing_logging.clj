(ns n-tools-dev.testing-logging
  (:require [n-tools-dev.init-log4j2]
            [n-tools.log :as nlog]
            #_[n-tools-dev.log4j2 :as lconf]
            #_[clojure.tools.logging :as log])
  #_(:import [org.apache.logging.log4j Logger]
           [org.apache.logging.log4j LogManager]))


;;--------------------------------------------------------
;; For trying out logging
;; Example of using log4j2 (and slf4j)
;;
;; Logging levels:
;;       ALL < DEBUG < INFO < WARN < ERROR < FATAL < OFF
;;--------------------------------------------------------

(defn -main [& args]

  ;; Fn println will only print to console (without using log4j2)
  ;; (println "Ok")

  ;; Raw:
  ;; (do
  ;;   (def log (. LogManager getLogger "main"))
  ;;   (. log info "Using java log4j2 classes directly from clojure.")
  ;;   (. log warn "Here a warning message."))

  
  ;;Optional: Setup log dir and level (note: has some requirements for log4j2.xml).
  ;; (lconf/setup-log-config :log-level "debug"
  ;;                         :log-dir "/tmp/logs/n-tools/"
  ;;                         :log4j2xml-sys-var-log-dir "log_directory"
  ;;                         :log4j2xml-sys-var-log-level "log_level")

  ;; With clojure.tools.logging (it interfaces with slf4j->log4j2):
  (nlog/testing-n-tools-dot-log-fns)
  (nlog/testing-clj-logging))
