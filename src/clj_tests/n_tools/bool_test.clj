(ns n-tools.bool-test
  (:require [clojure.test :refer :all]
            [n-tools.bool :refer [any-true? all-true]]))



;;--------------------------------------------------------
;; Bool fns
;;--------------------------------------------------------

(defn any-true?-test []
  (testing "any-true? fn..."
    (are [bool xs]
        (= bool (any-true? xs))
      nil [false nil false nil]
      ;;FIX: ERROR! 
      ;;false []
      true [true]
      true [false true])))

(defn all-true-test []
  (testing "all-true function... "
    (are [bool xs]
        (= bool (all-true xs))
      false [false nil false nil]
      ;;FIX
      ;;nil []
      true [true]
      false [false true])))

(deftest bool-tests
  "Testing all bool functions"
  (any-true?-test)
  (all-true-test))
