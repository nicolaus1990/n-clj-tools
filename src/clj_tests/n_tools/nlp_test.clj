(ns n-tools.nlp-test
  (:require [n-tools.nlp :as nnlp]
            ;;[clojure.string :as s]
            [clojure.test :refer :all]))

(def s1 "One... ")
(def s2 "Two. ")
(def s3 "Three and four! ")
(def s4 "And why not a five?? ")
(def s5 "And a six!! ")
(def s6 "Seven\n        ")
(def s7 "Newlines should be treated as end of sentence (regardless if there is a space after newline)\n")
(def s8 "Ok?")
(def sentence (str s1 s2 s3 s4 s5 s6 s7 s8))

(deftest iter-next-sentence-test
  (testing "Testing iter-next-sentence (with newline as punctuation)."
    (is (= {:remaining-str (str "Two. Three and four! And why not a five?? "
                                "And a six!! Seven\n        Newlines should be treated as end of "
                                "sentence (regardless if there is a space after newline)\nOk?"),
            :sentence "One... ", :sentence-num 1}
           (first
            (nnlp/iter-next-sentence sentence))))
    (let [parse-sentence-seq
          (nnlp/iter-next-sentence sentence :newline-as-punct true)]
      (are [num-of-iteration s-num s r-str]
          (= {:remaining-str r-str :sentence s :sentence-num s-num}
             (nth parse-sentence-seq num-of-iteration))
        0 1 s1 (str s2 s3 s4 s5 s6 s7 s8)
        1 2 s2 (str s3 s4 s5 s6 s7 s8)
        2 3 s3 (str s4 s5 s6 s7 s8)
        3 4 s4 (str s5 s6 s7 s8)
        4 5 s5 (str s6 s7 s8)
        5 6 s6 (str s7 s8)
        6 7 s7 (str s8)
        7 8 s8 ""
        )
      (is (thrown? IndexOutOfBoundsException (nth parse-sentence-seq 8))))))
