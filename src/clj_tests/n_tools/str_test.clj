(ns n-tools.str-test
  (:require [n-tools.str :refer [str->int
                                 has-http-protocol?
                                 is-hyper-link?]]
            ;;[clojure.string :as s]
            [clojure.test :refer :all]))

;;--------------------------------------------------------
;; String tests
;;--------------------------------------------------------


(deftest str->int-test
  "Testing str->int function"
  (are [num someStr]
      (= num (str->int someStr))
    0 "0"
    1 "1"
    124 "124"
    -100 "-100"))

;;--------------------------------------------------------
;; Regex tests
;;--------------------------------------------------------

(defn has-http-protocol?-test []
  (testing "Testing has-http-protocol? fn."
    (is (= nil (has-http-protocol? "htstp")))
    (is (= nil (has-http-protocol? "http://Something")))
    (is (not (= nil (has-http-protocol? "http://Something.net"))))))

(defn is-hyper-link?-test []
  (testing "Testing is-hyper-link? fn."
    (is (= nil (is-hyper-link? "htstp")))
    (is (= nil (is-hyper-link? "http://Something")))
    (is (not (= nil (is-hyper-link? "http://Something.net"))))))


(deftest regexes-test
  "Regex tests"
  (has-http-protocol?-test)
  (is-hyper-link?-test))



