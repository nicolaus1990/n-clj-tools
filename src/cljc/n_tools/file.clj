(ns n-tools.file
  #_(:import [java.util.zip ZipFile])
  (:require [n-tools.date :as date]
            [n-tools.str :as ns]
            [clojure.java.io :as io]
            [clojure.string :as str]))

(def sys-sep (java.io.File/separator))

(defn get-file-name
  ([file-path] (get-file-name file-path true))
  ([file-path with-extension]
   (let [file-with-ext
         (-> file-path
             (str/split (re-pattern (java.util.regex.Pattern/quote sys-sep)))
             (last))]
     (if with-extension file-with-ext
         (-> file-with-ext
             (str/split #"\.")
             (first))))))

(defn file-exists? [file-str] (.exists (io/as-file file-str)))

(defn hidden? [^java.io.File file]
  ;Requires: file to be java.io/file
  (.isHidden file))

;; isSymbolicLink(java.io.File file) is the same as: 
;; if (file == null)
;;   throw new NullPointerException("File must not be null");
;;   File canon;
;;   if (file.getParent() == null) {
;;     canon = file;
;;   } else {
;;     File canonDir = file.getParentFile().getCanonicalFile();
;;     canon = new File(canonDir, file.getName());
;;   }
;;   return !canon.getCanonicalFile().equals(canon.getAbsoluteFile());
;; }
;; Credit: https://stackoverflow.com/questions/813710/java-1-6-determine-symbolic-links
(defn isSymbolicLink? [^java.io.File file]
  ;Requires: file needs to be a java.io.file
  (let [canon (if (nil? (.getParent file))
                file
                (io/file (.getCanonicalFile (.getParentFile file))
                         (.getName file)))]
    (not (.equals (.getCanonicalFile canon)
                  (.getAbsoluteFile canon)))))

;;; Example:
;;; (seq2file "~/Desktop/myFile.txt"
;;;           "My header"
;;;          ["a\nb" "c"])
;;; --> Will write to myFile: "My header <DATE>\na\nbc"
(defn seq2file
  ;; Writes sequence of 'objs'  to file (appends).
  ;; Every elem of seq is written.
  ([file seq header] (seq2file file seq header true))
  ([file seq header in-new-line]
   (with-open [^java.io.BufferedWriter w (io/writer  file :append true)]
     (.write w (str header
                    (new java.util.Date) "\n"))
     (doseq [elem seq]
       (.write w (str elem (if in-new-line "\n" "")))))))

(defn make-folder-paths [{:keys [file-name extension
                                 path with-date-folder]
                          :or {file-name "tmp-file"
                               extension "csv"
                               path "/tmp/test-make-folder-paths/"
                               with-date-folder false}}]
  "Creates necessary parent dirs and returns string that is path to file.
Params: 
- date-folder can be boolean or a string-date (note: a date for a folder name)."
  (let [path (str path
                  (if (not with-date-folder) ""
                      (if (boolean? with-date-folder)
                        (str (date/get-date-str) "/")
                        (str with-date-folder    "/")))
                  file-name
                  "." extension)]
    (do (io/make-parents path)
        path)))

;;-------------------------------------------------------------------
;;  _____ ____  _   _    __                  _   _
;; | ____|  _ \| \ | |  / _|_   _ _ __   ___| |_(_) ___  _ __  ___
;; |  _| | | | |  \| | | |_| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
;; | |___| |_| | |\  | |  _| |_| | | | | (__| |_| | (_) | | | \__ \
;; |_____|____/|_| \_| |_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
;;-------------------------------------------------------------------


#_(defn deserialize-edn-file 
  ;; From https://github.com/clojure-cookbook/clojure-cookbook/blob/master/04_local-io/4-14_read-write-clojure-data-structures.asciidoc
  #_(-> filepath io/reader  (java.io.PushbackReader.) (clojure.edn/read))
  ([filepath] (deserialize-edn-file filepath {}))
  ([filepath {:keys [trust-edn-source
                     edn-read-opts]
              :or {trust-edn-source false
                   edn-read-opts {:eof nil}}}]
   (with-open [r (java.io.PushbackReader. (clojure.java.io/reader filepath))]
     (binding [*read-eval* false]
       ;; Note: In clojure there is clojure.core/read (for trusted sources)
       ;; and clojure.edn/read for datastructure interop and more.
       (if trust-edn-source 
         (read r false :end)
         (clojure.edn/read edn-read-opts r))))))

(defn deserialize-edn-file 
  ;; From https://github.com/clojure-cookbook/clojure-cookbook/blob/master/04_local-io/4-14_read-write-clojure-data-structures.asciidoc
  #_(-> filepath io/reader  (java.io.PushbackReader.) (clojure.edn/read))
  ([filepath] (deserialize-edn-file {} filepath))
  ([{:keys [trust-edn-source
            edn-read-opts]
     :or {trust-edn-source false
          edn-read-opts {:eof nil}}} filepath]
   (with-open [r (java.io.PushbackReader. (clojure.java.io/reader filepath))]
     (binding [*read-eval* false]
       ;; Note: In clojure there is clojure.core/read (for trusted sources)
       ;; and clojure.edn/read for datastructure interop and more.
       (if trust-edn-source
         (read r false ::EOF)
         ;; TODO: If edn has many objs then this should work but
         ;; an EOF exception keeps being thrown.
         #_(take-while #(not= ::EOF %) (repeatedly #(read r false ::EOF)))
         (throw (Exception. (str "Fn n-tools/deserialize-edn-file: "
                                 "Not implemented yet for untrusted edn sources.")))
         #_(clojure.edn/read edn-read-opts r))))))


(defn serialize-edn [file-str large-data]
  ;; From https://github.com/clojure-cookbook/clojure-cookbook/blob/master/04_local-io/4-14_read-write-clojure-data-structures.asciidoc
  (with-open [w (clojure.java.io/writer file-str)]
    (binding [*out* w]
      (pr large-data))))


