(ns n-tools.core
  (:require [n-tools.bool :as b]
            [n-tools.str :as s]
            ;;[n-tools.css.class-utils :as css]
            [n-tools.coll :as c]))

;;--------------------------------------------------------
;; For ____________ require _______________________ 
;;   - log            -->   n-tools.log
;;   - class-utils    -->   n-tools.css.class-utils
;;
;;--------------------------------------------------------


;;--------------------------------------------------------
;; Primitive fns
;;--------------------------------------------------------

(def any-true? b/any-true?)
(def all-true b/all-true)


;;--------------------------------------------------------
;; String tools
;;--------------------------------------------------------

(def str->int s/str->int)

(def subs* s/subs*)

(def split-and-do s/split-and-do)

(def repeatStr s/repeatStr )

(def rand-str2 s/rand-str2)

(def rand-str s/rand-str)


;; Regex tools

(def is-hyper-link? s/is-hyper-link?)

(def has-http-protocol? s/has-http-protocol?)


;;--------------------------------------------------------
;; Collection tools
;;--------------------------------------------------------

(def key->key-without-hyphen     c/key->key-without-hyphen)
(def key->key-without-underscore c/key->key-without-underscore)

(def add-first-vec c/add-first-vec)

(def vec-contains? c/vec-contains?)

(def into+ c/into+)

(def index-of c/index-of)

(def convert-to-vec c/convert-to-vec)

(def convert-map-to-vec-for-input c/convert-map-to-vec-for-input)

(def remove-nil-entries-in-map c/remove-nil-entries-in-map)
(def map->map-non-nil-vals c/map->map-non-nil-vals) ;; Note: does the same as remove-nil-entries-in-map

(def non-nil-keys c/non-nil-keys)
(def map->map-keys-no-hyphen     c/map->map-keys-no-hyphen)
(def map->map-keys-no-underscore c/map->map-keys-no-underscore)

(def map->map-without-vals c/map->map-without-vals)
(def map->map-non-false-vals c/map->map-non-false-vals)
(def map-trim c/map-trim)

(def remove-vec-elem c/remove-vec-elem)

(def add-vec-elem c/add-vec-elem)

(def vector-move c/vector-move)

(def dissoc-in c/dissoc-in)

(def dissoc-in+ c/dissoc-in+)

(def get-deep-elem c/get-deep-elem)

(def key-paths c/key-paths)

(def kv-paths-all c/kv-paths-all)

(def get-path-to c/get-path-to)
