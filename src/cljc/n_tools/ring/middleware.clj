(ns n-tools.ring.middleware
  (:require [n-tools.core :as t]
            [n-tools.log :as tl]
            [clojure.tools.logging :as log]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.middleware.not-modified :refer [wrap-not-modified]]
            ;; [ring.middleware.reload :refer [wrap-reload]]
            ;; [ring.middleware.json :refer [wrap-json-response]]
            ;; [ring.middleware.defaults :as rmid]
            ;; [ring.util.response :refer [response status resource-response content-type not-found]]
            ;; [compojure.core :refer [defroutes context routes
            ;;                         GET PUT POST DELETE ANY]]
            ;;[clj-http.client :as client]
))

;;---------------------------------------------------------------
;; Logging
;;---------------------------------------------------------------


(defn log-middleware-only-request
  ;;Middleware to debug request
  ([handler] (log-middleware-only-request handler "RECEIVED: "))
  ([handler prefixReq]
   (fn [request]
     (log/debug (str prefixReq (tl/pformat request)))
     (handler request))))

(defn log-middleware
  ;;Middleware to debug request and check response
  ([handler] (log-middleware handler "RECEIVED: " "ANSWERED: "))
  ([handler prefixReq prefixRes]
   (fn [request]
     (let [response (handler request)]
       (log/debug (str prefixReq (tl/pformat request)))
       (log/debug (str prefixRes (tl/pformat response)))
       response))))

;;---------------------------------------------------------------
;; Wrap resources 
;;---------------------------------------------------------------

(defn wrap-static-files [handler & {:keys [resource-path]
                                    :or {resource-path "public"}}]
  "Middleware: 3 middlewares: wrap-resource, wrap-content-type and
  wrap-not-modified."
  (-> handler
      (wrap-resource resource-path)
      (wrap-content-type)
      (wrap-not-modified)))

