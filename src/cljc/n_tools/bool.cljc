(ns n-tools.bool
    (:require [clojure.set :as cljSet]))



;;--------------------------------------------------------
;; Primitive fns
;;--------------------------------------------------------

(defn any-true? [xs] 
  ;;Never use 'any?' (because it always returns true).
  (some identity xs))
(defn all-true [xs]
  (apply = true xs))


(defn boolean-str-coerce [s]
  (if (boolean? s) s
      (= s "true")))

(defn give-nil-if-empty-str [s]
  (if (= "" s) nil s))
