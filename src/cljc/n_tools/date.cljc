(ns n-tools.date
  (:require [clojure.string :as str]
            #?(:clj [clojure.pprint :as pprint])
            [n-tools.log :as log]))

(:clj
 (defn get-date-str
   ([] (get-date-str "yyyy-MM-dd_hh-mm-ss"))
   ([date-format]
    #_(.toString (java.util.Date.))
    (.format (java.text.SimpleDateFormat. date-format) (java.util.Date.))))
 )
