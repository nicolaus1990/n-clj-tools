(ns n-tools.coll
  (:require [clojure.set :as cset]
            [clojure.string :as cstr]))


;;--------------------------------------------------------
;; Keys tools
;;--------------------------------------------------------

(defn key->key-without-hyphen [k]
  (keyword (cstr/replace (name k) #"-" "_")))

#_(comment (= :key_without_hyphens
              (keyword (cstr/replace (name :key-without-hyphens) #"-" "_"))))


(defn key->key-without-underscore [k]
  (keyword (cstr/replace (name k) #"_" "-")))

#_(comment (= :key-with-hyphens
              (keyword (cstr/replace (name :key_with_hyphens) #"_" "-"))))


;;--------------------------------------------------------
;; Collection tools
;;--------------------------------------------------------


(defn add-first-vec [target addition]
  "Helpful fn for conj element at beginning of vector target.

Based in the example from the great book CLOJURE for the BRAVE and TRUE
https://www.braveclojure.com/core-functions-in-depth/"
  (apply conj (if (vector? addition) addition [addition]) target))


(defn vec-contains? [vec elem]
  "Returns true if vector has a certain element? 

Note that clojure.core/contains works in this manner:

(contains? [:a :b :c] :b)  ;=> false
(contains? [:a :b :c] 2)   ;=> true

Whereas:

(vec-contains? [:a :b :c] :b)  ;=> true

"
  ;;(.contains vec elem)
  (some #(= % elem) vec))


(defn- into+--inner [acc arrs]
  (if (empty? arrs) acc
      (recur (into acc (first arrs)) (rest arrs))))
(defn into+ [& arrs]
  "Concatenates vectors"
  (into+--inner [] arrs))

#_(comment (into+ [1 2 3] [4 5 6] [7 8 9]))

(defn index-of #?(:clj [^java.util.List vector val]
                  :cljs [vector val])
  "Returns index of value, or -1 if not there."
  (.indexOf vector val))


(defn convert-to-vec [coll] (into [] coll))

(defn convert-map-to-vec-for-input [m]
  "When trying to pass key-vals map from another function, this converts previous map accordingly. 

For example:

(defn sum-num-to-a-b [num & {:keys [a b] :or {a nil b nil} :as opts}]
  (+ num (:a opts) (:b opts)))

(defn fn2 [& {:keys [a b] :or {a nil b nil} :as opts}]
  (apply (partial sum-num-to-a-b 10) (convert-map-to-vec-for-input opts)))

> (fn2 :a 2 :b 3) ---> 15
"
  (interleave (keys m) (vals m)))


(defn remove-nil-entries-in-map [m]
  "Example: {:a 1 :b nil :c 1} -> {:a 1, :c 1}"
  (select-keys m (for [[k v] m :when (not (nil? v))] k)))

(def map->map-non-nil-vals remove-nil-entries-in-map)
#_(defn map->map-non-nil-vals [m]
  (map->map-without-vals m #(nil? %))
  #_(comment "Above is exaclty like:")
  #_(apply dissoc                                                                                            
         m                                                                                                  
         (for [[k v] m :when (nil? v)] k)))


(defn non-nil-keys [check-keys kvals]
  "Given map `kvals` and a sequence of keys `check-keys`, returns sequence of
  keys that are in `check-keys` and are not nil."
  (->> kvals
       remove-nil-entries-in-map
       (map (fn [[k _]] (if (some #(= k %) check-keys) k nil)))
       (filter keyword?)))

#_(comment (= [:a :b]
              (non-nil-keys [:a :b :c] {:a 1 :b 2 :c nil :d 4} )))

(defn map->map-keys-no-hyphen [kvals]
  (cset/rename-keys kvals
                    (reduce (fn [acc [k v]]
                              (assoc acc k (key->key-without-hyphen k)))
                            {}
                            kvals)))

#_(comment (= {:a 1, :b 2, :c_hyphen nil, :d_many_hyphens false}
              (map->map-keys-no-hyphen {:a 1 :b 2 :c-hyphen nil :d-many-hyphens false})))



(defn map->map-keys-no-underscore [kvals]
  (cset/rename-keys kvals
                    (reduce (fn [acc [k v]]
                              (assoc acc k (key->key-without-underscore k)))
                            {}
                            kvals)))

#_(comment (= {:a 1, :b 2, :c-hyphen nil, :d-many-underscores false :e-another nil}
              (map->map-keys-no-underscore {:a 1 :b 2 :c-hyphen nil :d_many_underscores false :e_another nil})))

(defn map->map-without-vals [m & pred-fns]
  "Removes entries in map which return at least true for one predicate fn in `pred-fns`.

Note: allows persistent data sharing (uses dissoc) instead of creating a whole
new map (into).

Taken and modified from: https://stackoverflow.com/questions/3937661/remove-nil-values-from-a-map"
  (apply dissoc                                                                                            
         m                                                                                                  
         (for [[k v] m :when (reduce #(if (%2 v) (reduced true) false) false pred-fns)] k)))



(defn map->map-non-false-vals [m]
  (map->map-without-vals m #(false? %)))

(defn map-trim [m]
  "Will remove entries in map `m` where values are false or nil."
  (map->map-without-vals m #(nil? %) #(false? %)))

#_(comment
  (= (map-trim {:a 1 :b 2 :c nil :d false})
     {:a 1 :b 2})
  )


(defn remove-vec-elem [vec index]
  ;; Fn concat returns a lazy sequence.
  ;; Return a vector with (into [] ...) (convert-to-vec)
  (-> (concat (subvec vec 0 index)
              (subvec vec (inc index)))
      convert-to-vec))

(defn add-vec-elem [vec index elem]
  (-> (concat (subvec vec 0 index)
              [elem]
              (subvec vec index))
      convert-to-vec))

;; vector-move fn taken from: https://github.com/reagent-project/reagent/blob/master/examples/react-sortable-hoc/src/example/core.cljs
(defn vector-move [coll prev-index new-index]
  ;; Move elements in a vector (functional)
  (let [items (into (subvec coll 0 prev-index)
                    (subvec coll (inc prev-index)))]
    (-> (subvec items 0 new-index)
        (conj (get coll prev-index))
        (into (subvec items new-index)))))

#_(comment
  (= [0 2 3 4 1 5] (vector-move [0 1 2 3 4 5] 1 4)))


(defn dissoc-in
  "Dissociates an entry from a nested associative structure returning a new
  nested structure. keys is a sequence of keys. Any empty maps that result
  will not be present in the new structure.

  Taken from:
  https://github.com/clojure/core.incubator/blob/4f31a7e176fcf4cc2be65589be113fc082243f5b/src/main/clojure/clojure/core/incubator.clj#L63-L75
  See also:
  https://stackoverflow.com/questions/14488150/how-to-write-a-dissoc-in-command-for-clojure"
  [m [k & ks :as keys]]
  (if ks
    (if-let [nextmap (get m k)]
      (let [newmap (dissoc-in nextmap ks)]
        (if (seq newmap)
          (assoc m k newmap)
          (dissoc m k)))
      m)
    (dissoc m k)))

(defn dissoc-in+
  "Just like dissoc-in, but if last coll is vector and next key is number,
  removes that elem in vector at index (key)."
  [m [k & ks :as keys]]
  (if ks
    (if-let [nextmap (get m k)]
      (let [newmap (dissoc-in+ nextmap ks)]
        (if (seq newmap)
          (assoc m k newmap)
          (dissoc m k)))
      m)
    (if (and (vector? m) (integer? k))
      (remove-vec-elem m k)
      (dissoc m k))))






(defn get-deep-elem [xss comp-fn get-xs-fn]
  ;; Get deep nested element from some obj xss
  (if (comp-fn xss)
    xss
    (if (empty? (get-xs-fn xss))
      nil
      (some (fn [xs] (get-deep-elem xs comp-fn get-xs-fn)) (get-xs-fn xss)))))






;; Fns key-paths and kv-paths-all are from miner49r: https://stackoverflow.com/questions/21768802/how-can-i-get-the-nested-keys-of-a-map-in-clojure
(defn key-paths
  ;; Like kv-paths-all but only for maps
  ([m] (key-paths [] m ()))
  ([prev m result]
   (reduce-kv (fn [res k v] (if (map? v)
                              (key-paths (conj prev k) v res)
                              (conj res (conj prev k))))
              result
              m)))

(defn kv-paths-all
  ;; Returns vector with keys to leaf-nodes. Helpful to get path to some part of a deep
  ;; nested map (to be updated with update-in fn, for example).
  ([m] (kv-paths-all [] m ()))
  ([prev m result]
   (reduce-kv (fn [res k v] (if (associative? v)
                              (let [kp (conj prev k)]
                                (kv-paths-all kp v (conj res kp)))
                              (conj res (conj prev k))))
              result
              m)))

#_(comment (= (kv-paths-all {:a 1 :b 2 :c {:d 3 :e [100 200 300]}})
            [[:c :e 2] [:c :e 1] [:c :e 0] [:c :e] [:c :d] [:c] [:b] [:a]]))




(defn get-path-to
  ;; Returns vector with keys of a specific leaf-node. Helpful to get path to some part of a deep
  ;; nested map (to be updated with update-in fn, for example).
  ;; Note: it will return the initial coll xs, if (comp-fn xs) is true.
  ([comp-fn m]
   (if (comp-fn m) [] (::result-of-get-path-to (get-path-to comp-fn [] m ()))))
  ([comp-fn prev m result]
   (reduce-kv (fn [res k v]
                (do ;;(println (str "res:"res " k:"k " v:"v))
                    (if (get v ::result-of-get-path-to)
                      (reduced v)
                      (if (comp-fn v)
                        (reduced {::result-of-get-path-to (conj prev k)})
                        (if (associative? v)
                          (let [kp (conj prev k)]
                            (get-path-to comp-fn kp v (conj res kp)))
                          res;;[] #_(conj res (conj prev k))
                          )))
                    ))
              result
              m)))

#_(comment (= (get-path-to #(= % 10) {:a 1 :b {:b2 2 :b3 [1 10 100]} :c 3 :d 4})
            [:b :b3 1])
         (= (get-path-to #(= % 59) {:a 1 :b {:b2 2 :b3 [1 10 100]} :c 3 :d 4})
            nil)
         (= (get-path-to #(= (:id %) "some-id") {:a 1 :b {:id "some-id" :b2 2 :b3 [1 10 100]} :c 3 :d 4})
            [:b])
         (= (get-path-to #(= (:id %) "some-id") {:id "some-id" :b {:b2 3} :c "C"})
            {:id "some-id" :b {:b2 3} :c "C"})
         (= (get-path-to #(= (:id %) "some-id") {:id "not-the-id" :b {:b2 [{:b3 "B3"} {:id "some-id"}]} :c "C"})
            [:b :b2 1])
         ;;Example from app:
         (def some-state
           {:id "root"
            :xs [{:id (rand-str) :x "List 1" :xs nil}
                 {:id (rand-str) :x "List 2" :xs [{:id "Some random str" :x "List 2.1" :xs [{:id (rand-str) :x "List 2.1.1" :xs nil}
                                                                                     {:id (rand-str) :x "List 2.1.2" :xs [{:id (rand-str) :x "List 2.1.2.1" :xs nil}
                                                                                                                          {:id (rand-str) :x "List 2.1.2.2" :xs nil}]}]}]}
                 {:id (rand-str) :x "List 3" :xs [{:id (rand-str) :x "List 3.1" :xs nil}
                                                  {:id (rand-str) :x "List 3.2" :xs nil}]}
                 {:id (rand-str) :x "List 4" :xs [{:id (rand-str) :x "List 4.1" :xs [{:id (rand-str) :x "List 4.1.1" :xs nil}
                                                                                     {:id (rand-str) :x "List 4.1.2" :xs nil}]}]}]})
         (get-path-to #(= (:id %) "Some random str") some-state)

         (def items ({:x "a", :link-mode nil} {:x "b", :link-mode nil}))
         (def some-s
           {:id "n-app-lists list-id of root node", :x "n-app-lists: All lists",
            :xs [{:id "1", :x "List 1",
                  :xs [{:id "2", :x "List 2.1", :xs [{:id "3", :x "List 2.1.1", :xs nil}]}]}]})
         (def list-id "n-app-lists list-id of root node")
         (defn get-path-to-list [list-obj list-id] (get-path-to #(= (:id %) list-id) list-obj))
         (get-path-to-list some-s list-id))


;; Checking fns reduce, reduced and reduce-kw
;; reduced := Wraps x in a way such that a reduce will terminate with the value x
#_(comment (= (reduce (fn [a v] (do (println (str "a:"a " v:"v))
                                  (if (< a 11) (+ a v) (reduced :big))))
                    0 [1 2 3 4 5])
            15)
         (= (reduce (fn [a v] (do (println (str "a:"a " v:"v))
                                  (if (< a 10) (+ a v) (reduced :big))))
                    0 [1 2 3 4 5])
            :big)
         ;; Notice how reduce works: printlns will be spared (does not go traverse sequence completely).
         (reduce (fn [a v] (do (println (str "a:"a " v:"v))
                               (if (< a 5) (+ a v) (reduced :big))))
                 0 [1 2 3 4 5])
         ;; Trying out reduce-kv with reduced fn. (Result: works!)
         (= (reduce-kv (fn [a k v] (do (println (str "a:"a " v:"v " k:"k))
                                       (if (< a 11) (+ a v) (reduced :big))))
                       0 [1 2 3 4 5])
            15)
         (= (reduce-kv (fn [a k v] (do (println (str "a:"a " v:"v " k:"k))
                                       (if (< a 10) (+ a v) (reduced :big))))
                       0 [1 2 3 4 5])
            :big)
         ;;Checking if reduce-kv also does not go to the end of sequence. (Result: reduced works with reduce-kv too).
         (reduce-kv (fn [a k v] (do (println (str "a:"a " v:"v " k:"k))
                                    (if (< a 5) (+ a v) (reduced :big))))
                    0 [1 2 3 4 5])
         )





