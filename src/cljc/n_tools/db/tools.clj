(ns n-tools.db.tools
  (:require ;;These ring deps came (probably?) with figwheel.
            ;;[n-tools.core :as t]
            [n-tools.coll :as tc]
            ;;[clojure.data.json :as json]
            [clojure.java.jdbc :as jdbc]
            [clojure.string :as str]
            [clojure.set :as cset]
            [clojure.tools.logging :as log]
            [clojure.pprint :as pprint]))

;;-------------- SQL-DB tools

(defn table-exists? [db-spec table-name]
  ((comp not empty?)
    ;;This expr should return (if table-name exists) something like:
    ;; ({:table_cat nil, :table_schem "public", :table_name "<table-name>",
    ;; :table_type "TABLE", :remarks nil})
   (jdbc/with-db-metadata [md db-spec]
     (jdbc/metadata-result (.getTables md nil nil table-name nil
                                       ;;(into-array ["TABLE" "VIEW"])
                                       )))))

;;-----------------------------------------
;;   ____                _       
;;  / ___|_ __ ___  __ _| |_ ___ 
;; | |   | '__/ _ \/ _` | __/ _ \
;; | |___| | |  __/ (_| | ||  __/
;;  \____|_|  \___|\__,_|\__\___|
;;-----------------------------------------


(defn create-table 
  [& {:keys [db-spec table-name ddl drop-if-exists]
      :or {db-spec false, table-name false, ddl false, drop-if-exists false}
      :as all}]
  "Will create table, but checking if table exists first. If it exists, then
  does nothing. If drop-if-exists is true, then it will drop table (cascade)
  and create it."
  (when (or (false? db-spec) (false? table-name) (false? ddl))
    (throw (Exception. (str "Fn create-table wrong input: " all "\n"
                            "Required arguments: db-spec, table-name and ddl"))))
  (if drop-if-exists
    (let [sql-exprs [(str  "DROP TABLE IF EXISTS " table-name " CASCADE")
                     ddl]]
      (jdbc/db-do-commands db-spec true sql-exprs))
    (if-not (table-exists? db-spec table-name)
      (jdbc/db-do-commands db-spec true [ddl])
      ;;(println (str "Table " table-name " exists already."))
      (log/debug (str "Table " table-name " exists already."))
      )))


;;-------------------------------------------------------------
;;   ___                  _           
;;  / _ \ _   _  ___ _ __(_) ___  ___ 
;; | | | | | | |/ _ \ '__| |/ _ \/ __|
;; | |_| | |_| |  __/ |  | |  __/\__ \
;;  \__\_\\__,_|\___|_|  |_|\___||___/
;;-------------------------------------------------------------


(defn query-table [db-spec & {:keys [select-attrs
                                     table-name
                                     from-other-query
                                     where-attrs
                                     limit
                                     offset
                                     order-by-attrs
                                     only-query-str
                                     other-query-name]
                              :or {select-attrs "*"
                                   table-name ""
                                   from-other-query ""
                                   where-attrs []
                                   limit nil
                                   offset nil
                                   order-by-attrs {}
                                   only-query-str false
                                   other-query-name "from_other_query"}}]
  (let* [select-clause (str "SELECT " select-attrs " ")
         from-clause (if (empty? from-other-query) (str "FROM " table-name " ")
                         (str "FROM (" from-other-query ") AS " other-query-name " "))
         where-clause (if (empty? where-attrs) ""
                          (->> (map (fn [cond] (str " " cond " "))
                                    where-attrs)
                               (str/join " AND ")
                               (str "WHERE ")))
         order-by-clause (if (empty? order-by-attrs) ""
                             (->> (map (fn [key-and-val] (str (name (first key-and-val)) " "
                                                              (name (second key-and-val)) " "))
                                   order-by-attrs)
                                  (str/join ", ")
                                  (str " ORDER BY ")))
         limit-clause (if (not limit) ""
                          (str " LIMIT " limit (if (not offset) ""
                                                   (str " OFFSET " offset))))
         query-str (str select-clause
                        from-clause
                        where-clause
                        order-by-clause
                        limit-clause)]
    (if only-query-str
      query-str
      (jdbc/query db-spec [query-str]))))



(defn query-from-other-query [from-other-query db-spec & {:keys [select-attrs
                                                                 where-attrs
                                                                 limit
                                                                 offset
                                                                 order-by-attrs
                                                                 only-query-str
                                                                 other-query-name]
                                                          :or {select-attrs "*"
                                                               where-attrs []
                                                               limit nil
                                                               offset nil
                                                               order-by-attrs {}
                                                               only-query-str false
                                                               other-query-name "from_other_query"}
                                                          :as opts}]
  "Convenient function to wrap previous query with. 

Example: 
     (-> (query-table ... :only-query-str true) 
         (query-from-other-query db-spec ...))
"
  (apply (partial query-table db-spec) (-> (assoc opts
                                                  :from-other-query from-other-query)
                                           tc/convert-map-to-vec-for-input)))

                               
;;----------------------------------------------
;;  ___                     _   _             
;; |_ _|_ __  ___  ___ _ __| |_(_) ___  _ __  
;;  | || '_ \/ __|/ _ \ '__| __| |/ _ \| '_ \ 
;;  | || | | \__ \  __/ |  | |_| | (_) | | | |
;; |___|_| |_|___/\___|_|   \__|_|\___/|_| |_|
;;----------------------------------------------

(defn get-record-or-insert [table-name db-spec attr-values
                            & {:keys [select-attrs
                                      insert-attr-vals
                                      only-query-str]
                               :or {select-attrs "*"
                                    insert-attr-vals {}
                                    only-query-str false}}]
  "Will query if ATTR-VALUES (map: key is attr name, value is attr value) is
  in TABLE-NAME, and return its query. If not there will insert them (and
  returns whatever came from jdbc/insert!). If values to insert aren't the
  same for searching, use optional parameter INSERT-ATTR-VALS."
  (let* [select-clause (str "SELECT " select-attrs " ")
         where-clause (if (empty? attr-values) ""
                          (->> (map (fn [key-and-val] (str (name (first key-and-val)) "='"
                                                           (second key-and-val) "' "))
                                   attr-values)
                               (str/join " AND ")
                               (str "WHERE ")))
         query-str (str select-clause
                        "FROM " table-name " "
                        where-clause)]
    (if only-query-str
      query-str
      (let [result-set (jdbc/query db-spec [query-str])]
        (if (first result-set)
          result-set
          (jdbc/insert! db-spec table-name
                        (if (empty? insert-attr-vals)
                          attr-values
                          insert-attr-vals)))))))

(defn drop-table-content
  ([db table-name]
   (jdbc/with-db-connection [conn db]
     ;;(jdbc/delete! conn "sayings")
     (jdbc/execute! conn ;;["DELETE FROM app_lists"]
                    [(str "DELETE FROM " table-name)]
                    ))))

;; --- SQLite specific

(defn make-spec-sqlite [pathToDB]
  {:classname   "org.sqlite.JDBC"
   :subprotocol "sqlite"
   :subname     pathToDB})

;; Gets last generated rowid from given resultset.
;; res: The result of a jdbc/insert (lazy sequence)
;; Sqlite specific (Postgres handles it differently
(defn getGenId [res]
  ((first (doall res))
   ;Using "keyword" necessary because keyword 'last_inse..' has parenthesis.
   (keyword "last_insert_rowid()")))
