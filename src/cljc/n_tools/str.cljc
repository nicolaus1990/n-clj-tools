(ns n-tools.str
  (:require [clojure.string :as str]
            #?(:clj [clojure.pprint :as pprint])
            [n-tools.log :as log]))


;;--------------------------------------------------------
;; String tools
;;--------------------------------------------------------

(defn first-letter-str [s] (subs s 0 1))

;; (defn strIndexOf [long-str sub-str]
;;   ;; Get index of start of sub-str in long-str.
;;   (.indexOf long-str sub-str))

(defn str->int [s]
  #?(:clj  (java.lang.Integer/parseInt s)
     :cljs (js/parseInt s)))

(defn subs* [s start end]
  "Like subs but allows negative indeces."
  (let [new-s (if (neg? start) (+ (count s) start) start)
        new-e (if (neg? end) (+ (count s) end) end)]
    (if (< new-s new-e)
      (subs s new-s new-e)
      (subs s new-e new-s))))

(defn split-and-do [s do-fn]
  "Splits string s on newline or \r\n and does do-fn for every line."
  (->> s
       str/split-lines
       (map do-fn)))

(defn repeatStr [someStr numtimes]
  (apply str (repeat numtimes someStr)))

(defn rand-str2 [len]
  (apply str (take len (repeatedly #(char (+ (rand 26) 65))))))

(defn rand-str
  ([] (rand-str 10))
  ([len]
   (apply str (take len (repeatedly #(char (+ (rand 26) 65)))))))

(defn str-contains #?(:clj [^String some-str elem]
                      :cljs [some-str elem])
  (.contains some-str elem))





;;--------------------------------------------------------
;; Not in core.cljc
;;--------------------------------------------------------

(defn pformat [& args]
  "Pretty print but *out* is Stringbuffer and not stdout."
  #?(:clj (with-out-str
            (apply pprint/pprint args))
     :cljs (apply str args)))


(defn map->str
  ;; {:id 1 :name "Bob" ...} --> "# :id:1 # :name:Bob\n"
  ;; delim: key/val delimiter, joinVals: in which way to join the key/val pairs.
  ;; prefix: A string to append before each key-value pair (default: empty str).
  ([m]
   (map->str m " " " " ""))
  ([m joinVals]
   (map->str m joinVals " " ""))
  ([m joinVals delim]
   (map->str m joinVals " " ""))
  ([m joinVals delim prefix]
   (str (str/join joinVals
                  (map #(str prefix %1 delim %2) (keys m) (vals m)))
        "\n")))



(defn shrink-sentence #?(:clj [^Long max-len ^String sentence ^String word]
                         :cljs [max-len sentence word])
  "Given sentence and word, will shrink sentence such that its length is at most max-len and word is in it.
It requires that all words in sentence are separated by a single space and no more.

Note: it will swallow extra spaces, if words are separated with more than one single-space."
  (if (<= (count sentence) max-len)
    ;; Return sentence
    sentence ;; To also be able to return nil (if word is not there): (and (index-of sentence word) sentence)
    (if (< max-len (count word)) nil
        (let #?(:clj [^java.util.List sentence-vec (str/split sentence #"[ ]+")];; Ignore double-spaces
                :cljs [sentence-vec (str/split sentence #"[ ]+")])
          (let* [;;^java.util.List sentence-vec (str/split sentence #"[ ]+")
                 s-vec-len (count sentence-vec)
                 word-len (count word)
                 word-vec-index (.indexOf sentence-vec word)
                 toggle-left-right #(if (= % :right) :left :right)
                 ;; Fns for loop (further down). We split sentence into a vector
                 ;; of words. We keep taking more and more words to the right
                 ;; and to the left (hence two indices: right and left) of WORD.
                 ;; Fn to check if index is out of bounds (depending if it is right or left index.
                 index-in-bound (fn [r-or-l index] (if (= r-or-l :right)
                                                     (< index s-vec-len)
                                                     (not (< index 0))))
                 ;; Fn get-available-chars computes new available chars if word at index i-new-word is added
                 get-available-chars (fn [chars-free word-index-to-add]
                                       ;; Substract from available chars the length of sentence to add.
                                       (- chars-free
                                          (count (get sentence-vec word-index-to-add))
                                          ;; We also have to take into account the space betweeen words (ie:
                                          ;; between vector elems). Each time we take a word, we suppose that there
                                          ;; is a single-space in between.
                                          1
                                          ))
                 ;; Computes answer when recursive-loop finishes (see further below) 
                 get-ans-str (fn [left-i right-i]
                               (if (= left-i right-i) (get sentence-vec left-i)
                                   (str/join " " (subvec sentence-vec left-i (inc right-i)))))
                 chars-available (- max-len word-len)
                 ]
            (if (= -1 word-vec-index) nil
                (loop [left-vec-index word-vec-index
                       right-vec-index word-vec-index
                       ca chars-available
                       r-or-l :right
                       right-left-done? nil]
                  (do #_(log/debug-strs "lvi: " left-vec-index ", rvi: " right-vec-index
                                 ", ca: " ca ", r-or-l: " r-or-l
                                 ", rl-done?:" right-left-done?)
                      (if (= right-left-done? :both)
                        (get-ans-str left-vec-index right-vec-index) #_(log/spy (get-ans-str left-vec-index right-vec-index))
                        #_(get-ans-str left-vec-index right-vec-index)
                        (let* [new-lvi (let [next-index (dec left-vec-index)]
                                         (if (and (= r-or-l :left)
                                                  (index-in-bound :left next-index)
                                                  (not (< (get-available-chars ca next-index) 0)))
                                           next-index
                                           left-vec-index))
                               new-rvi (let [next-index (inc right-vec-index)]
                                         (if (and (= r-or-l :right)
                                                  (index-in-bound :right next-index)
                                                  (not (< (get-available-chars ca next-index) 0)))
                                           next-index
                                           right-vec-index))
                               new-ca (or (and (= new-lvi left-vec-index) (= new-rvi right-vec-index) ca)
                                          (get-available-chars ca (if (not (= new-lvi left-vec-index))
                                                                    new-lvi
                                                                    new-rvi)))
                               new-r-l-done? (or (and (= new-lvi left-vec-index)
                                                      (= new-rvi right-vec-index)
                                                      ;; If one index is moved as further as possible and
                                                      ;; it was turn to move the other and no change in index was detected,
                                                      ;; then it means it's over.
                                                      (or (and (= r-or-l :left) (= right-left-done? :right-done))
                                                          (and (= r-or-l :right) (= right-left-done? :left-done)))
                                                      :both)
                                                 (and (= r-or-l :right)
                                                      (= new-rvi right-vec-index)
                                                      (not (= right-left-done? :left-done))
                                                      :right-done)
                                                 (and (= r-or-l :left)
                                                      (= new-lvi left-vec-index)
                                                      (not (= right-left-done? :right-done))
                                                      :left-done)
                                                 ;; Else: stays the same
                                                 right-left-done?)
                               ]
                          (recur new-lvi
                                 new-rvi
                                 new-ca
                                 (toggle-left-right r-or-l)
                                 new-r-l-done?)))))))))))

(comment

  (and

   (=  (shrink-sentence 300 "any string" "ignored-word") "any string")
   (=  (shrink-sentence 1 "any string" "relevant-word") nil)
   (=  (shrink-sentence 300 "any string with relevant-word" "relevant-word") "any string with relevant-word")
   (=  (shrink-sentence 13 "any string with relevant-word" "relevant-word") "relevant-word")
   (=  (shrink-sentence 18 "any string with relevant-word" "relevant-word") "with relevant-word")
   (=  (shrink-sentence 18 "     Notice    spaces     are     counted    as     one. " "spaces") " Notice spaces are")
   
   (=  (shrink-sentence 8 "1 22 333" "22") "1 22 333")
   (=  (shrink-sentence 2 "1 22 333" "22") "22")
   (=  (shrink-sentence 3 "1 22 333" "22") "22")
   (=  (shrink-sentence 4 "1 22 333" "22") "1 22")
   (=  (shrink-sentence 8 "1 22 333" "333") "1 22 333")
   (=  (shrink-sentence 3 "1 22 333" "333") "333")
   (=  (shrink-sentence 5 "1 22 333" "333") "333")
   (=  (shrink-sentence 6 "1 22 333" "333") "22 333")

   (=  (shrink-sentence 8 "1 22 333 88888888 4444 " "333") "1 22 333")
   (=  (shrink-sentence 8 "1 22 88888888 333 22 1" "333") "333 22 1")
   (=  (shrink-sentence 8 "1 22 88888888 333 22 22 0" "0") "22 22 0")
   )
  )


(defn shrink-sentence-with-ellipsis [max-len sentence word]
  "Like shrink-sentence but will pre- and posfix elipsis ('...')."
  (str "..."
       (shrink-sentence (- max-len 6) sentence word)
       "..."))


;;--------------------------------------------------------
;; Regex tools
;;--------------------------------------------------------

(def regex-capital-letter #"[A-ZÁÉÍÓÚÄÖÜ]")

(def regex-english-letter #"[a-zA-Z]")
(def regex-spanish-letter #"[a-zA-ZñÁÉÍÓÚáéíóúü]")
(def regex-german-letter #"[a-zA-ZÄÖÜäöüß]")
(def regex-letter-eng-span-deu #"[a-zA-ZñÁÉÍÓÚáéíóúöüß]")
(def regex-unicode-letter #"\p{L}")
(def regex-unicode-letter-and-some-special-chars #"[\p{L}']")

(def regex-latin-word #"\p{IsLatin}+")
(def regex-unicode-word #"\p{L}+")




(defn has-http-protocol? [s]
  (re-seq #"^(http|https)://(www\.)?.+\..*$" s))

(defn is-hyper-link? [s]
  (re-seq #"^(http|https|ftp)://(www\.)?.+\.(com|org|net|io|info|int|edu|gov|de|tv)(/|$)?.*$" s))


(defn TokenizeJackLine [LineOfJackFile]
  ;; Remove? What does this do?
  ;; TODO: clj differs with the cljs implementation
  #?(:clj
     (filter not-empty 
             (->
              (str/trim LineOfJackFile)
              ;; get rid of all comments
              (str/replace #"(//.*)|(\s*/?\*.*?($|\*/))|([^/\*]*\*/)" "") 
              ;; split into tokens using 0-width look-ahead
              (str/split #"(?<=[\{\}\(\)\[\]\.,;+\-\*/&\|<>=~])|(?=[\{\}\(\)\[\]\.,;+\-\*/&\|<>=~])")))
     :cljs
     (filter not-empty 
             (->
              (str/trim LineOfJackFile)
              ;; get rid of all comments
              #_(str/replace #"(//.*)|(\s*/?\*.*?($|\*/))|([^/\*]*\*/)" "")
              ;; Note: We can not use look-ahead in javascript, else we might
              ;; get the following error when compiling:
              ;; 'ERROR: JSC_UNTRANSPILABLE. Cannot convert ECMASCRIPT_2018
              ;; feature "RegExp Lookbehind" to targeted output language.'
              ;;TODO: Do something similar as in clj implementation.
              (str/split #"[\W\s]+")
              ))))
