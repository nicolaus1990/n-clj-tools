(ns n-tools.log
  (:require #?(:clj [clojure.tools.logging :as log])
            ;;[n-tools.str :refer [repeatStr]]
            #?(:clj [clojure.pprint :as pprint])))

;;--------------------------------------------------------
;; Helper functions
;;
;; Do require only what is necessary (avoid circular dependencies). It is
;; intended that n-tools.log is possible to be required by every other
;; namespace.
;;--------------------------------------------------------

(defn- strs-to-str [txts]
  (apply str txts))

(defn- repeatStr [someStr numtimes]
  (apply str (repeat numtimes someStr)))

(declare debug-strs)
(defn- log-not-implemented [& {:keys [prefix suffix]
                              :or {prefix "", suffix ""}
                              :as all}]
  (debug-strs prefix
              "------------   NOT IMPLEMENTED IN THIS PLATFORM!   ------------"
              suffix))

;;--------------------------------------------------------
;; Log tools
;;
;; Logging levels:
;;       ALL < DEBUG < INFO < WARN < ERROR < FATAL < OFF
;;
;;--------------------------------------------------------

#?(:clj (defn- log-inner ([type msg] (log-inner type msg nil))
          ([type msg error]
           (case type
             :alert (log-inner :error msg error)
             :debug (log/debug msg)
             :info (log/info msg)
             :warn (log/warn msg)
             ;; For error, check first if it is not nil to be passed as message.
             :error (if error (log/error error msg) (log/error msg))
             :fatal (log/fatal msg)
             ;;Default: ;;(println (apply str txts))
             (log/error (str "log-inner wrong argument! Message was:" msg)))))
   :cljs (defn- log-inner [type msg]
           (if (= :alert type)
             (js/alert msg)
             ;;Ignore type. Just log
             (.log js/console msg))))

;;--------------------------------------------------------
;; Interface
;;--------------------------------------------------------

(defn debug [msg] (log-inner :debug msg))
(defn info [msg] (log-inner :info msg))
(defn warn [msg] (log-inner :warn msg))
#?(:clj (defn error ([msg] (error msg nil))
          ([error msg] (log-inner :error msg error)))
   :cljs (defn error [msg] (log-inner :error msg)))
(defn fatal [msg] (log-inner :fatal msg))
;; To be used in cljs:
(defn alert [msg] (log-inner :alert msg))

;; previously: log
(defn log-strs [& txts] (debug (strs-to-str txts)))
;; previously: log-2
(defn log-str-vec [txts] (apply log-strs txts))

(defn spy [smt-to-print-and-return] (do (debug smt-to-print-and-return) smt-to-print-and-return))

(defn spy-with [smt-to-print-and-return & {:keys [prefix suffix]
                                           :or {prefix "", suffix ""}}]
  (do (log-strs prefix smt-to-print-and-return suffix) smt-to-print-and-return))

(defn spy-with->> [{:keys [prefix suffix]
                    :or {prefix "", suffix ""}} obj]
  (spy-with obj :prefix prefix :suffix suffix))


(defn log-ind [indentation & txts]
  ;;Log txt with indentation. (Useful fn for recursion).
  ;;#?(:cljs (.log js/console (str (repeatStr "     " indentation) (apply str txts))))
  (debug (str (repeatStr "     " indentation) (strs-to-str txts))))


(defn info-strs [& txts] (info (strs-to-str txts)))
(defn debug-strs [& txts] (debug (strs-to-str txts)))
(defn error-strs [& txts] (error (strs-to-str txts)))
(defn alert-strs [& txts] (alert (strs-to-str txts)))


;;---------------------

(defn pformat [& args]
  "Pretty print but *out* is Stringbuffer and not stdout.
Argument args is either an object or an object and a writer."
  ;;Pretty print but *out* is Stringbuffer and not stdout.
  #?(:clj (with-out-str
            (apply pprint/pprint args))
     :cljs (log-not-implemented)))



(defn testing-n-tools-dot-log-fns []
  "Testing logging fns of n-tools.log namespace."
  (info "**************************************************")
  (info "BEGINNING n-tools.log LOGGING TEST")
  (debug "DEBUG msg")
  (info "INFO msg")
  (warn "WARN msg")
  (error "ERROR msg")
  #?(:clj (try (throw (Exception. "Specific exception."))
               (catch Exception e
                 (error e (str "Some exception was thrown. Message: " (.getMessage e)))))
     :cljs (log-not-implemented :prefix "Fn error with error obj not available in cljs."))
  (fatal "FATAL msg")
  (alert "Alert message (intended for cljs)")
  (log-strs "Hi." "There." "Fn log-strs.")
  (log-str-vec ["Hi." "There." "Fn log-str-vec."])
  (debug (str "Will repeat map "(spy {:num-times 2})))
  (log-strs "And again: Will repeat map "
            (spy-with {:num-times 2}
                      :prefix "Some prefix: "
                      :suffix " and some postfix."))
  (spy-with {:num-times 2}
            :suffix "<-- This time. prefix was not given! ")
  (log-strs "Map: "
            (->> {:num-times 2}
                 (spy-with->> {:prefix "This time, map was given as last arg. Map:"})))
  (log-ind 4 "Will " "indent " "4" " spaces.")
  (debug-strs "Hi." "There." "Fn debug-strs.")
  (error-strs "Hi." "There." "Fn error-strs.")
  (alert-strs "Hi." "There." "Fn alert-strs.")
  (pformat {:a "1" :b "2"})
  (info "ENDING n-tools.log LOGGING TEST")
  (info "**************************************************"))

#?(:clj
   (defn testing-clj-logging []
     "For trying out logging of clojure.tools.logging"
     (log/info "**************************************************")
     (log/info "BEGINNING CLJ LOGGING TEST")
     (log/info "Some info msg.")
     (log/debug "Some debug msg.")
     (log/warn "Some warn msg.")
     (log/error "Some error msg.")
     (try (/ 1 0)
          (catch Exception e
            (log/error e "Catched exception!")))
     (log/fatal "Some fatal msg.")
     
     (log/debug "Spying expression returned: "
                (str "..."
                     (log/spy "Long sentence without ellipsis, once, with ellispis later.")
                     "..."))
     ;; And pretty print some:
     (log/infof " " {:a [1 2] :b [3 4]})
     
     ;; For trying out rolling file appender (before set in xml also SizeBasedTriggeringPolicy size to "1MB")
     (doseq [n (range 10)]
       (log/debug "Log msg number:" n))
     (log/info "END OF CLJ LOGGGING TEST")
     (log/info "**************************************************")))


;;--------------------------------------------------------
;; Deprecated
;;--------------------------------------------------------


#_(defn log-debug [& rest]
  (do (log "DEBUG:\n" (strs-to-str rest))))

#_(defn log-info [& rest]
  (do (log "INFO:\n" (strs-to-str rest))))

#_(defn log-ret [something & {:keys [prefix]}]
  (do (log  prefix " " something) something))
