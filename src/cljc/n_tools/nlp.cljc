(ns n-tools.nlp
  (:require [clojure.string :as str]
            #?(:clj [clojure.pprint :as pprint])
            [n-tools.log :as log]))

(defn get-punctuations [long-str & {:keys [newline-as-punct]}]
  "Returns a sequence of punctuations in long-str. Each elem of seq is a
string of punctuations (either one of '.!?').

If optional newline-as-punct given, then will treat newline as a punctuation
mark.
"
  (let [regex-puncts (if newline-as-punct
                       #"[\.\!\?]+\s+$?|\n+\s*$?"
                       #"[\.\!\?]+\s+$?")]
    (re-seq regex-puncts long-str)))


(defn get-punct-pos [long-str nearest-puncts]
  "Returns punct-pos: nearest index of long-str where nearest-puncts starts."
  ;; TODO: Add ; punct to the list
  ;;This only returns pos of nearest punctuation. We want all concatenated
  ;;punctuation marks.
  #_(let [len (count long-str)]
    (reduce #(min % (or (str/index-of long-str %2) len))
            len
            ".!?;"))
  ;; This punctuations should only be at the end of sentence (not beginning or
  ;; elsewhere) and at least a space.
  #_(if-let [nearest-puncts (first (get-punctuations long-str :newline-as-punct newline-as-punct))])
  (let [index (str/index-of long-str nearest-puncts)
        puncts-len (count nearest-puncts)]
    (dec (+ index puncts-len))))

(defn get-sentence [long-str punct-pos]
  "If punct-pos from get-punct-pos, then it shoud return the first sentence in long-str."
  (subs long-str 0
        ;; punct-pos is allowed to be zero or length of long-str (we add one
        ;; to punct-pos to keep punctuation mark).
        (if (= (count long-str) punct-pos) punct-pos (+ 1 punct-pos))))

(defn get-next-sentence [long-str]
  (let [punct-pos (get-punct-pos long-str (first (get-punctuations long-str)))]
    {:punct-pos (or punct-pos (count long-str))
     :sentence (get-sentence long-str punct-pos)}))


(defn- iter-next-sentence-inner [long-str sentence-num punctuations]
  (iterate (fn [{:keys [remaining-str sentence punct-pos punctuations] :as kwargs}]
             ;;To return also last remaining-str
             (if (:almost-finished kwargs)
               (assoc kwargs :finished true)
               ;; TODO: In fn punct-pos, re-seq regex-puncts long-str is being run all the time... 
               ;; The output of it is all the punctuation marks.
               (if-let [next-punct (first punctuations)]
                 (let [punct-pos (get-punct-pos remaining-str next-punct)]
                   (-> kwargs
                       (update :remaining-str #(subs %
                                                     (if (= (count %) punct-pos)
                                                       punct-pos (+ 1 punct-pos))))
                       (assoc :sentence (get-sentence remaining-str punct-pos)
                              :punctuations (rest punctuations))
                       (update :sentence-num inc)
                       #_{:remaining-str (subs remaining-str
                                               (if (= (count remaining-str) punct-pos) punct-pos (+ 1 punct-pos)))
                          :punct-pos punct-pos
                          :sentence (get-sentence remaining-str punct-pos)}))
                 (-> kwargs
                     (assoc :almost-finished true
                            :remaining-str ""
                            :sentence remaining-str)
                     (update :sentence-num inc)))))
           {:remaining-str long-str :sentence-num sentence-num
            :sentence :starting :finished false
            :punctuations punctuations}))


(comment
  (let [long-str "One... Two. Three and four! And why not a five?? And a six!! Seven"]
    (take 8 (iter-next-sentence-inner long-str
                                      0
                                      (get-punctuations long-str)))))

#_(defn iter-next-sentence [text]
  (for [{:keys [sentence finished] :as kwargs} (iter-next-sentence-inner text 0)
        :when (not= sentence :starting)
        :while (not finished)]
    (dissoc kwargs :finished)))

(defn iter-next-sentence [text & {:keys [newline-as-punct]}]
  (let [punctuations (get-punctuations text :newline-as-punct newline-as-punct)]
    (->> (iter-next-sentence-inner text 0 punctuations)
         (filter #(not= :starting (:sentence %)))
         (take-while #(not (:finished %)))
         (map #(dissoc % :almost-finished :finished :punctuations)))))

(comment
  (iter-next-sentence "One... Two. Three and four! And why not a five?? And a six!! Seven"))
