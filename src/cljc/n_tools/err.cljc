(ns n-tools.err
  (:require [n-tools.coll :as tc]))

(defn throw-error-if [not-allowed-results err-message f & args]
  "Wraps function to throw error if result after applying function it returns
  a non-allowed result, else return result.

Use like: (partial throw-error-if [<NOT ALLOWED VALS>] \"Error msg!\" some-function)"
  #?(:clj
     (let [result (apply f args)]
       (if (tc/vec-contains? not-allowed-results result)
         (throw (Exception. (str err-message " not-allowed-results: " not-allowed-results " and result: " result)))
         result))))
