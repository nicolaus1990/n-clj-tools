;; This test runner is intended to be run from the command line
(ns n-tools.test-runner
  (:require [figwheel.main.testing :refer [run-tests-async]]
            ;; require all the namespaces that you want to test
            [n-tools.core-test]
            [n-tools.tools.tools-test]))

;; This isn't strictly necessary, but is a good idea depending
;; upon your application's ultimate runtime engine.
;;(enable-console-print!)

(defn -main [& args]
  (run-tests-async 5000))
