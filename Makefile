.PHONY: clean build

all: build

build: clean
	lein do clean, cljsbuild once min

test:
	lein with-profile +dev test

test-hot-reload:
	lein figwheel

clean:
	lein clean

help:
	lein run -h
